function reduce(items, callBack, startingValue){
   
    if(startingValue === undefined){
        startingValue = items[0];
    }else{
        startingValue = callBack(startingValue,items[0],0,items);
    }

    if(items !== [] && Array.isArray(items)){

        for (let index=1; index<items.length; index++) {
            startingValue = callBack(startingValue, items[index], index, items);
        }
        
    }
    return startingValue;

}

module.exports = reduce;