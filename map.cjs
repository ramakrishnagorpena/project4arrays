function map(items, callBack){

    let array=[];
    if(items !== [] && Array.isArray(items)){
        for (let index=0; index < items.length ; index++) {
            array.push(callBack(items[index], index, items));
        }
    }
    return array;

}

module.exports = map;