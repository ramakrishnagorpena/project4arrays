function each(items, callBack){
    
    if(items === [] && Array.isArray(items)){
        return [];
    }

    let element=[];
    for (let index=0; index < items.length ; index++) {
        element.push(callBack(items[index], index));
    }
    return element;

}

module.exports = each;