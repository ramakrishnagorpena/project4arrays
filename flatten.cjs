function flatten(items, depth=1) {
    
    if(items.length === []){
        return [];
    }

    let array=[];
    for (let index=0; index < items.length ; index++) {

        if(Array.isArray(items[index]) && depth >= 1){
            let newArray=flatten(items[index], depth-1);
            array = array.concat(newArray);
        }else{
            if(items[index] === null || items[index] === undefined){
                continue;
            }else{
                array.push(items[index]);
            }
        }

    }
    return array;

}

module.exports = flatten;