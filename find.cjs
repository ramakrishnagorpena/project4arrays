function find(items, callBack){

    if(items !== [] && Array.isArray(items)){

        for (let index=0; index < items.length ; index++) {

            if(callBack(items[index])===true){
                return items[index];
            }
        }
        return undefined;
    }

}

module.exports = find;