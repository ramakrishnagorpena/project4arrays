const result = require('./flatten.cjs');

const items = [1, [2], [[3]], [[[4]]]];
const flattenFunction = result(items, 2);

console.log(flattenFunction);