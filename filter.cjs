function filter(items, callBack){

    if(items !== [] && Array.isArray(items)){
        let array=[];
        for (let index=0; index < items.length ; index++) {

            if(callBack(items[index], index, items)===true){
                array.push(items[index]);
            }
        }
        return array;
    }
}

module.exports = filter;